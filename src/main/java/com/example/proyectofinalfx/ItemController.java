package com.example.proyectofinalfx;

import chinchilla.josue.bl.entities.Administrador.Administrador;
import chinchilla.josue.bl.entities.Item.Item;
import chinchilla.josue.bl.logic.GestorAdministrador;
import chinchilla.josue.bl.logic.GestorItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.FileChooser;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

public class ItemController {

    FileChooser fileChooser = new FileChooser();


    GestorItem gestor = new GestorItem();

        @FXML
        private Button btnCategoria;

        @FXML
        private TableColumn<Item, String> fechaCol;

        @FXML
        private TextField descripcionItem;

        @FXML
        private TableColumn<Item, String> antiguedadCol;

        @FXML
        private TextField estadoItem;

        @FXML
        private Button btnProcesarItem;

        @FXML
        private TableColumn<Item, String> categoriaCol;

        @FXML
        private TextField categoriaItem;

        @FXML
        private TextField antiguedadItem;

        @FXML
        private ImageView imageView;

        @FXML
        private TextField nombreItem;

        @FXML
        private TextField codigoItem;

        @FXML
        private TableColumn<Item, String> estadoCol;

        @FXML
        private TextField fechaCompraItem;

        @FXML
        private TableColumn<Item, String> descripcionCol;

        @FXML
        private TableView<Item> tablaItems;

        @FXML
        private TableColumn<Item, String> nombreCol;

    @FXML
    private TableColumn<Item, String> codigoCol;
        @FXML
        private Button btnRefrescarItems;

        @FXML
        private Button subirImagen;



        @FXML
        void addInfoItems(ActionEvent event) throws SQLException, ClassNotFoundException {

            String nombre = nombreItem.getText();
            String descripcion = descripcionItem.getText();
            String estado = estadoItem.getText();
            String fechaCompra = fechaCompraItem.getText();
            String antiguedad = antiguedadItem.getText();
            String categoria = categoriaItem.getText();
            String codigo = codigoItem.getText();
            Object evt =  event.getSource();

            if (evt.equals(btnProcesarItem)){
                JOptionPane.showMessageDialog(null, gestor.almacenarItem(nombre, descripcion, estado, fechaCompra, antiguedad, categoria, codigo));
            }

        }

ObservableList<Item> listaItems = FXCollections.observableArrayList();
    private void cargarDatosItems() throws Exception {

            gestor.getListaItems().forEach(item -> listaItems.addAll(item));
        nombreCol.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        descripcionCol.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
        estadoCol.setCellValueFactory(new PropertyValueFactory<>("estado"));
        fechaCol.setCellValueFactory(new PropertyValueFactory<>("fechaCompra"));
        antiguedadCol.setCellValueFactory(new PropertyValueFactory<>("antiguedad"));
        categoriaCol.setCellValueFactory(new PropertyValueFactory<>("categoria"));
        codigoCol.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        tablaItems.setItems(listaItems);

    }

/*
    private ObservableList<Item> datos() throws Exception{
        ObservableList<Item> items = FXCollections.observableArrayList();
        for (Item itemTemporal : gestor.getListaItems()) {
            items.add(itemTemporal);
        }
        return items;
    }
*/

    @FXML
    void eventAction(ActionEvent event) throws Exception {

        Object evt = event.getSource();

        if (evt.equals(btnRefrescarItems)) {
            cargarDatosItems();
        }
    }



        @FXML
        void handleImage(ActionEvent event) {

            fileChooser.getExtensionFilters().clear();
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));

            File file = fileChooser.showOpenDialog(null);
            if (file != null){
                imageView.setImage(new Image(file.toURI().toString()));
            } else {
                System.out.println("El formato de la imagen es incorrecto");
            }

        }




    



    private Stage stage;
    private Scene scene;
    private Parent root;




    @FXML
    void switchToAdmins(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("administrador.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchToCategories(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("categorias.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchToColeccionistas(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("hello-view.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchToSubasta(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("subasta.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }


}
