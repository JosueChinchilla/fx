package com.example.proyectofinalfx;

import chinchilla.josue.bl.entities.Administrador.Administrador;
import chinchilla.josue.bl.entities.Subasta.Subasta;
import chinchilla.josue.bl.logic.GestorSubasta;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.sql.SQLException;

public class SubastaController {

    GestorSubasta gestor = new GestorSubasta();


    @FXML
    private Button btnCategoria;

    @FXML
    private Button btnColeccionista;

    @FXML
    private TextField estadoSubasta;

    @FXML
    private Button btnRefrescarSubasta;

    @FXML
    private TableColumn<Subasta, String>  precioCol;

    @FXML
    private TableColumn<Subasta, String>  creacionCol;

    @FXML
    private TableColumn<Subasta, String>  finalCol;

    @FXML
    private TableColumn<Subasta, String>  estadoCol;

    @FXML
    private Button btnAdministrador;

    @FXML
    private Button btnProcesarSubasta;

    @FXML
    private TextField nombreVendedor;

    @FXML
    private Button btnItems;

    @FXML
    private TextField fechaVence;

    @FXML
    private TextField fechaInicio;

    @FXML
    private TableView<Subasta> tablaSubastas;

    @FXML
    private TableColumn<Subasta, String> nombreCol;

    @FXML
    private TableColumn<Subasta, String>  inicioCol;

    @FXML
    private TextField precioMínimo;

    @FXML
    private TextField fechaCreacion;

    @FXML
    private TableColumn<Subasta, String> itemsCol;

    @FXML
    private TextField itemsSubasta;


    @FXML
    void addInfoSubasta(ActionEvent event) throws SQLException, ClassNotFoundException {
        String nombreVen = nombreVendedor.getText();
        String fechaIni = fechaInicio.getText();
        String fechaFin = fechaVence.getText();
        String estado = estadoSubasta.getText();
        String fechaCrea = fechaCreacion.getText();
        String precioMin = precioMínimo.getText();
        String items = itemsSubasta.getText();

        Object evt = event.getSource();

        if (evt.equals(btnProcesarSubasta)){
            JOptionPane.showMessageDialog(null, gestor.almacenarSubasta(nombreVen, fechaIni, fechaFin, fechaCrea, precioMin, estado, items));
        }
    }


    ObservableList<Subasta> listaSubastas = FXCollections.observableArrayList();
    private void cargarDatosSubasta() throws Exception {

        gestor.getListaSubastas().forEach(subasta -> listaSubastas.addAll(subasta));
        nombreCol.setCellValueFactory(new PropertyValueFactory<>("nombreVendedor"));
        precioCol.setCellValueFactory(new PropertyValueFactory<>("precioMinimo"));
        inicioCol.setCellValueFactory(new PropertyValueFactory<>("fechaInicio"));
        finalCol.setCellValueFactory(new PropertyValueFactory<>("fechaFinal"));
        creacionCol.setCellValueFactory(new PropertyValueFactory<>("fechaCreacion"));
        estadoCol.setCellValueFactory(new PropertyValueFactory<>("estado"));
        itemsCol.setCellValueFactory(new PropertyValueFactory<>("items"));
        tablaSubastas.setItems(listaSubastas);

    }

    @FXML
    void eventAction(ActionEvent event) throws Exception {
        Object evt = event.getSource();

        if (evt.equals(btnRefrescarSubasta)) {
            cargarDatosSubasta();
        }
    }


    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    void switchToColeccionistas(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("hello-view.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchToCategories(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("categorias.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchToItems(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("item.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchToAdmin(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("administrador.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
