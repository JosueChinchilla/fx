package com.example.proyectofinalfx;


import chinchilla.josue.bl.entities.Administrador.Administrador;
import chinchilla.josue.bl.logic.GestorAdministrador;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;


public class AdminsController {

    GestorAdministrador gestor = new GestorAdministrador();


        @FXML
        private Button btnProcesarAdmin;

        @FXML
        private Button btnRefrescarAdmin;

        @FXML
        private PasswordField contrasenniaAdmin;

        @FXML
        private TextField correoAdmin;

        @FXML
        private TableColumn<Administrador, String> correoCol;

        @FXML
        private TextField direccionAdmin;

        @FXML
        private TableColumn<Administrador, String> direccionCol;

        @FXML
        private TextField edadAdmin;

        @FXML
        private TableColumn<Administrador, Integer> edadCol;

        @FXML
        private TextField estadoAdmin;


    @FXML
    private TableColumn<Administrador, String> fechaNacimientoCol;


    @FXML
        private TableColumn<Administrador, String> estadoCol;

        @FXML
        private TextField identificacionAdmin;

        @FXML
        private TextField fechaNacimientoAdmin;

        @FXML
        private TableColumn<Administrador, String> identificacionCol;

        @FXML
        private TextField nombreAdmin;

        @FXML
        private TableColumn<Administrador, String> nombreCol;

        @FXML
        private TableColumn<Administrador, String> contrasenniaCol;

        @FXML
        private TableView<Administrador> tablaAdministradores;


    @FXML
    public void addInfoAdministrador(ActionEvent event) throws SQLException, ClassNotFoundException {
        String nombre = nombreAdmin.getText();
        String identificacion = identificacionAdmin.getText();
        String correo = correoAdmin.getText();
        String estado = estadoAdmin.getText();
        String edad = edadAdmin.getText();
        String direccion = direccionAdmin.getText();
        String contrasennia = contrasenniaAdmin.getText();
        String fechaNacimiento = fechaNacimientoAdmin.getText();

        Object evt = event.getSource();

        if (evt.equals(btnProcesarAdmin)){
            JOptionPane.showMessageDialog(null, gestor.almacenarAdministrador( nombre,  direccion,  correo,  identificacion, edad, estado, contrasennia, fechaNacimiento));
        }
    }



    ObservableList<Administrador> listaAdministradores = FXCollections.observableArrayList();

    private void cargarDatosAdministrador() throws Exception {

        gestor.getListaAdministradores().forEach(administrador -> listaAdministradores.addAll(administrador));
            nombreCol.setCellValueFactory(new PropertyValueFactory<>("nombre"));
            direccionCol.setCellValueFactory(new PropertyValueFactory<>("direccion"));
            correoCol.setCellValueFactory(new PropertyValueFactory<>("correo"));
            identificacionCol.setCellValueFactory(new PropertyValueFactory<>("identificacion"));
            edadCol.setCellValueFactory(new PropertyValueFactory<>("edad"));
        estadoCol.setCellValueFactory(new PropertyValueFactory<>("estado"));
        contrasenniaCol.setCellValueFactory(new PropertyValueFactory<>("contrasennia"));
        fechaNacimientoCol.setCellValueFactory(new PropertyValueFactory<>("fechaNacimiento"));
            tablaAdministradores.setItems(listaAdministradores);

        }



    @FXML
    void eventAction(ActionEvent event) throws Exception {

        Object evt = event.getSource();

        if (evt.equals(btnRefrescarAdmin)) {
            cargarDatosAdministrador();
        }
    }



    private Stage stage;
    private Scene scene;
    private Parent root;




    @FXML
        void switchToAdmin(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("administrador.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        }

        @FXML
        void switchToCategories(ActionEvent event) throws IOException {
            Parent root = FXMLLoader.load(getClass().getResource("categorias.fxml"));
            stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }

        @FXML
        void switchToColeccionistas(ActionEvent event) throws IOException {
            Parent root = FXMLLoader.load(getClass().getResource("hello-view.fxml"));
            stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }

    public void switchToItems(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("item.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchToSubasta(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("subasta.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

}




