package com.example.proyectofinalfx;

import chinchilla.josue.bl.entities.Administrador.Administrador;
import chinchilla.josue.bl.logic.GestorColeccionista;
import chinchilla.josue.bl.entities.Coleccionista.Coleccionista;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.sql.SQLException;


public class HelloController {

    GestorColeccionista gestor = new GestorColeccionista();
    @FXML
    private TableColumn<Coleccionista, String> correoCol;

    @FXML
    private TableColumn<Coleccionista, String> edadCol;

    @FXML
    private TextField nombre;
    @FXML
    private TextField direccion;


    @FXML
    private TextField correo;


    @FXML
    private TextField identificacion;

    @FXML
    private TableColumn<Coleccionista, String> identificacionCol;

    @FXML
    private TextField fechaNacimientoColeccionista;


    @FXML
    private TextField edad;

    @FXML
    private TableColumn<Coleccionista, String> fechaNacimientoCol;

    @FXML
    private TableColumn<Coleccionista, String> direccionCol;

    @FXML
    private TableColumn<Coleccionista, String> nombreCol;

    @FXML
    private Button btnProcesar;

    @FXML
    private Button btnRefrescar;

    @FXML
    private Button btnColeccionista;

    @FXML
    private Button btnCategoria;

    @FXML
    private TableView<Coleccionista> tablaColeccionistas;

    @FXML
    void addInfo(ActionEvent event) throws SQLException, ClassNotFoundException {
        String nombreColeccionista = nombre.getText();
        String direccionColeccionista = direccion.getText();
        String correoColeccionista = correo.getText();
        String identificacionColeccionista = identificacion.getText();
        String fechaNacimiento = fechaNacimientoColeccionista.getText();
        String edadColeccionista = edad.getText();


        Object evt = event.getSource();

        if (evt.equals(btnProcesar)) {
            JOptionPane.showMessageDialog(null, gestor.registrarColeccionista(nombreColeccionista, direccionColeccionista, correoColeccionista, identificacionColeccionista, fechaNacimiento, edadColeccionista));

        }
    }
    ObservableList<Coleccionista> listaColeccionistas = FXCollections.observableArrayList();
    private void cargarDatos() throws Exception {

        gestor.getListaColeccionista().forEach(coleccionista -> listaColeccionistas.addAll(coleccionista));
        nombreCol.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        direccionCol.setCellValueFactory(new PropertyValueFactory<>("direccion"));
        correoCol.setCellValueFactory(new PropertyValueFactory<>("correo"));
        identificacionCol.setCellValueFactory(new PropertyValueFactory<>("identificacion"));
        fechaNacimientoCol.setCellValueFactory(new PropertyValueFactory<>("fechaNacimiento"));
        edadCol.setCellValueFactory(new PropertyValueFactory<>("edad"));
        tablaColeccionistas.setItems(listaColeccionistas);

    }

    @FXML
    private void eventAction(ActionEvent event) throws Exception {


        Object evt = event.getSource();

        if (evt.equals(btnRefrescar)) {
            cargarDatos();
        }
    }

    private Stage stage;
    private Scene scene;
    private Parent root;


    public void switchToColeccionistas(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("hello-view.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

   public void switchToCategories(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("categorias.fxml"));
       stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
   }

    public void switchToAdministadores(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("administrador.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void switchToItems(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("item.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchToSubasta(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("subasta.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

}
