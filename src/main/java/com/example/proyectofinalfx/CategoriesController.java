package com.example.proyectofinalfx;


import chinchilla.josue.bl.entities.Categoria.Categoria;
import chinchilla.josue.bl.logic.GestorCategoria;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.IOException;
import java.sql.SQLException;

public class CategoriesController {


    GestorCategoria gestor = new GestorCategoria();

    @FXML
    private TextField codigoCategoria;

    @FXML
    private TextField estadoCategoria;

    @FXML
    private TableColumn<Categoria, String> codigoCol;

    @FXML
    private TableColumn<Categoria, String> nombreCol;

    @FXML
    private Button btnRefrescarCategorias;

    @FXML
    private Button btnProcesarCategoria;

    @FXML
    private TableView<Categoria> tablaCategorias;


    @FXML
    private TextField nombreCategoria;

    @FXML
    private TableColumn<Categoria, String> estadoCol;



    @FXML
    public void addInfoCategoria(ActionEvent event) throws SQLException, ClassNotFoundException {
        String nombreCat = nombreCategoria.getText();
        String codigoCat = codigoCategoria.getText();
        String estadoCat= estadoCategoria.getText();
        Object evt = event.getSource();

        if (evt.equals(btnProcesarCategoria)) {
            JOptionPane.showMessageDialog(null, gestor.almacenarCategoria(nombreCat, codigoCat, estadoCat ));

        }
    }

    ObservableList<Categoria> listaCategorias = FXCollections.observableArrayList();

    private void cargarDatosCategoria() throws Exception {

        gestor.getListaCategorias().forEach(categoria -> listaCategorias.addAll(categoria));
        nombreCol.setCellValueFactory(new PropertyValueFactory<>("nombreCategoria"));
        codigoCol.setCellValueFactory(new PropertyValueFactory<>("codigoCategoria"));
        estadoCol.setCellValueFactory(new PropertyValueFactory<>("estadoCategoria"));

        tablaCategorias.setItems(listaCategorias);
    }

/*
    private ObservableList<Categoria> datos() throws Exception {
        ObservableList<Categoria> categorias = FXCollections.observableArrayList();
        for (Categoria categoriaTemporal : gestor.getListaCategorias()) {
            categorias.add(categoriaTemporal);
        }
        return categorias;
    }
    */

    @FXML
    void eventAction(ActionEvent event) throws Exception {

        Object evt = event.getSource();

        if (evt.equals(btnRefrescarCategorias)) {
            cargarDatosCategoria();
        }
    }


    private Stage stage;
    private Scene scene;
    private Parent root;


    public void switchToColeccionistas(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("hello-view.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void switchToCategories(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("categorias.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchToSubasta(ActionEvent event) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("subasta.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void switchToAdministadores(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("administrador.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void switchToItems(ActionEvent event) throws IOException{
        Parent root = FXMLLoader.load(getClass().getResource("item.fxml"));
        stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }


}
