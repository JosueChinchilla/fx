module com.example.proyectofinalfx {
    requires javafx.controls;
    requires javafx.fxml;
    requires ProyectoFinalBL;
    requires java.desktop;
    requires java.sql;

    opens com.example.proyectofinalfx to javafx.fxml;
    exports com.example.proyectofinalfx;
}